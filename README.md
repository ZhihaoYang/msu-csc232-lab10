# Lab 10 - Spending as little money as possible

## Background

Dynamic Programming has been introduced to us as a means of solving _optimization problems_. In this lab, we seek to find a solution to a problem wherein we seek an optimal path through a "liability matrix." Optimal, in this sense, means minimizing our "liability" by finding a path from the upper left-hand corner of this liability matrix (i.e., position (0,0)) to some general location (m, n) in the matrix. We are allowed to travel from (0, 0) to (m, n) by either moving to the right, or down, or diagonally in the matrix. That is, from a given cell (i, j), you can go to any of (i + 1, j), (i, j + 1), or (i + 1, j + 1) (which represent movements to the right, down and diagonally, respectively).
When we visit each cell, we incur a certain liability. The total liability in moving from (0, 0) to (m, n) is the sum of all the liabilities on that path (including both origin and destination). You may assume that all entries in the matrix are positive integers. For example, in the following weight matrix, what is the minimum liability in going from (0, 0) to (2, 2)?

```
1          2          3
4          8          2
1          5          3          
```

The path with the minimal liability would be (0, 0) -> (0, 1) -> (1, 2) -> (2, 2) with a liability of 8 (1 + 2 + 2 + 3).

This problem lends itself to a solution using Dynamic Programming as it exemplifies (1) optimal substructure and (2) overlapping subproblems.

### Optimal Substructure

The path to reach (m, n) must be through one of the 3 cells: (m-1, n-1) or (m-1, n) or (m, n-1). As such, the minimum liability to reach (m, n) can be written as "minimum of the 3 cells plus liability[m][n].

minLiability(m, n) = _min_(minLiability(m-1, n-1), minLiability(m-1, n), minLiability(m, n-1)) + liability[m][n]

### Overlapping Subproblems

Below is simple, recursive implementation of the MLP (Minimum Liability Path) problem. The implementation simply follows the recursive structure mentioned above.

```c++
/**
  * Returns cost of minimum liability path from (0,0) to (m, n) in the given  
  * liability matrix
  *
  * @param liabilityMatrix the given liability matrix
  * @param m the destination row of the given liability matrix
  * @param n the destination column of the given liability matrix
  * @return The minimum liability in traveling from (0, 0) to (m, n) is returned.
  * @pre If M is the number of rows in the given liability matrix and N is the
  *      maximum number of columns in the given liability matrix, then
  *      0 <= m <= M - 1
  *      0 <= n <= N - 1
  *
  *      A function named min that takes three integer arguments exists that
  *      computes (and returns) the minimum value of its three arguments exists.
  */
int minLiability(int** liabilityMatrix, int m, int n) {
   if (n < 0 || m < 0) {
      return -1; // a sentinel value indicating bad input
   } else if (m == 0 && n == 0) {
      // the simple, base case of a 1x1 liability matrix
      return liabilityMatrix[m][n];
   } else {}
      return liabilityMatrix[m][n] + min( minLiability(liabilityMatrix, m-1, n-1),
                                          minLiability(liabilityMatrix, m-1, n),
                                          minLiability(liabilityMatrix, m, n-1) );
   }
}
```

It should be noted that the above function computes the same subproblems again and again. See the following recursion tree, there are many nodes that appear more than once. Time complexity of this naïve recursive solution is exponential and it is terribly slow.

```
mL refers to minLiability()
                                    mL(2, 2)
                          /             |               \
                         /              |                 \             
                 mL(1, 1)           mL(1, 2)                mL(2, 1)
              /     |     \       /     |     \           /     |     \
             /      |      \     /      |      \         /      |       \
       mL(0,0) mL(0,1) mL(1,0) mL(0,1) mL(0,2) mL(1,1) mL(1,0) mL(1,1) mL(2,0)
```

Using Dynamic Programming techniques, we can reduce the time complexity of this recursive solution from being exponential to _O_(_m_*_n_).

## Tasks

As usual, fork this repo into your own private repo; clone it to your local machine and create a develop branch within which to do your work.

Like HW#4, you must fill in the code for the stubbed out methods found in lab10.cpp. Make the following changes, committing your code after each change with an appropriate commit message:

1. Add you and your partner(s) names to the header comments. Save your changes and compile. If there are no compiler errors, commit your change with an appropriate commit message.
1. Implement the display() function. Save your changes and compile. If there are no compiler errors, commit your change with an appropriate commit message.
1. Implement the min() function. Save your changes and compile. If there are no compiler errors, commit your change with an appropriate commit message.
1. Implement the minLiability() function. Save your changes and compile. If there are no compiler errors, commit your change with an appropriate commit message.

When you have completed these steps, create a pull request for merging your develop branch into your master branch and add your instructor (Jim Daehn) as the sole reviewer for the pull request. As your final step, each member of your team must submit this lab on Blackboard in the usual fashion, i.e., by supplying a text submission that contains a _working hyperlink_ to your pull request along with a list of the members of your team.
